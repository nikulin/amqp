# README #

`docker-compose up -d` should start project for you.
Note: this is PoC, you have to change watched directory on server manually.

You can also run client in foreground typing
```
docker exec -ti client python /client.py --host rabbitmq
```

#!/usr/bin/env python

import argparse
import json
import pika
import time

print(' [.] Client started...')

parser = argparse.ArgumentParser(description='Get directory info from server.')
parser.add_argument('--host', dest='host', default='localhost', help='Broker host')
parser.add_argument('--port', dest='port', default=5672, type=int, help='Broker port')
parser.add_argument('--queue', dest='queue', default='dir', help='Broker queue')
parser.add_argument('-n', dest='count', default=10, type=int, help='Max connections attempts')
parser.add_argument('-t', '--timeout', dest='timeout', default=1, type=int, help='Time between retries')

args = parser.parse_args()

connection = None
i = 1
while connection is None:
    if args.count > 0 and i > args.count:
        print(" [X] Can't connect to broker")
        exit(-1)

    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters(args.host, args.port))
    except pika.exceptions.AMQPConnectionError:
        print(f" [x] Attempt {i} failed")
        i += 1
    else:
        break
    time.sleep(args.timeout)
print(f' [V] got connection {connection}')

channel = connection.channel()

queue = channel.queue_declare(queue='', exclusive=True)
try:
    channel.queue_bind(exchange=args.queue, queue=queue.method.queue)
except pika.exceptions.ChannelClosedByBroker:
    print(f" [X] Can't connect to queue {args.queue}")

print(f' [V] got queue {queue.method.queue}')


def callback(ch, method, properties, body):
    print(" [.] Received update")
    print(json.loads(body))
    print()


try:
    channel.basic_consume(
        queue=queue.method.queue,
        auto_ack=True,
        on_message_callback=callback,
    )
except pika.exceptions.ChannelWrongStateError as e:
    print(f' [X] Channel is closed ("{e}")')
    exit(-2)

print(' [V] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
#!/usr/bin/env python

import argparse
import json
import os
import pika
import time

from watchdog.observers import Observer
from watchdog.events import EVENT_TYPE_MODIFIED, FileSystemEventHandler


class Broker:

    def __init__(self, channel, queue):
        self.channel = channel
        self.queue = queue


class Handler(FileSystemEventHandler):

    def __init__(self, broker, path):
        super().__init__()
        self.broker = broker
        self.path = path

    def on_any_event(self, event):
        if event.is_directory:
            return None

        if event.event_type == EVENT_TYPE_MODIFIED:
            return None

        self.broker.channel.basic_publish(
            exchange=self.broker.queue,
            routing_key='',
            body=json.dumps(os.listdir(self.path)),
            properties=pika.BasicProperties(
                delivery_mode=2,  # make message persistent
            ),
        )


class Watcher:

    def __init__(self, path, broker, check_interval=1):
        self.observer = Observer()
        self.path = path
        self.broker = broker
        self.check_interval = check_interval if check_interval > 0 else 1

    def run(self):
        event_handler = Handler(broker=self.broker, path=self.path)
        self.observer.schedule(event_handler, self.path, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(self.check_interval)
        except Exception as e:
            self.observer.stop()
            print(f'Error "{e}" occured')

        self.observer.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Directory watcher.')
    parser.add_argument('-d', '--dir', dest='path', default='/', help='Watched directory')
    parser.add_argument('--host', dest='host', default='localhost', help='Broker host')
    parser.add_argument('--queue', dest='queue', default='dir', help='Broker queue')
    parser.add_argument('--port', dest='port', default=5672, type=int, help='Broker port')
    parser.add_argument('-n', dest='count', default=10, type=int, help='Max connections attempts')
    parser.add_argument('-t', '--timeout', dest='timeout', default=1, type=int, help='Time between retries')

    args = parser.parse_args()

    print(f' [.] Server started. Watching "{args.path}" directory...')

    connection = None
    i = 1
    while connection is None:
        if args.count > 0 and i > args.count:
            print(" [X] Can't connect to broker")
            exit(-1)

        try:
            connection = pika.BlockingConnection(pika.ConnectionParameters(args.host, args.port))
        except pika.exceptions.AMQPConnectionError:
            print(f" [x] Attempt {i} failed")
            i += 1
        else:
            break
        time.sleep(args.timeout)
    print(f' [V] got connection {connection}')

    channel = connection.channel()
    channel.exchange_declare(exchange=args.queue, exchange_type='fanout')
    broker = Broker(channel=channel, queue=args.queue)

    watcher = Watcher(path=args.path or '/tmp/', broker=broker)
    watcher.run()

    connection.close()